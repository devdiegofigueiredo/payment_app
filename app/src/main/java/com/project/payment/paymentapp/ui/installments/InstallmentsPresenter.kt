package com.project.payment.paymentapp.ui.installments

import com.project.payment.paymentapp.ui.installments.domain.model.Installment

class InstallmentsPresenter(val view: InstallmentsContract.View,
                            val interactor: InstallmentsContract.Interactor) :
        InstallmentsContract.Presenter,
        InstallmentsContract.Presenter.InstallmentsCallback {

    override fun installments(paymentMethod: String,
                              bankId: String,
                              amount: String) {
        interactor.installments(this, paymentMethod, bankId, clearMoneyFormatt(amount))
        view.showProgress()
    }

    override fun onInstallmentsSuccess(installments: List<Installment>) {
        view.setupInstallments(installments)
        view.hideProgress()
    }

    override fun onInstallmentsError(message: String, buttonTitle: String) {
        view.showErrorView(message, buttonTitle)
        view.hideProgress()
    }

    override fun onInstallmentsConfirmed(installmentSelected: Installment.Installments) {
        view.showPaymentInformationDialog(installmentSelected)
    }

    override fun onPaymentInformationDialogConfirmed() {
        view.reestartPaymentFlow()
    }

    private fun clearMoneyFormatt(bankId: String): String {
        return bankId.substring(0, bankId.length - 2).replace("[R$,.]".toRegex(), "")
    }
}