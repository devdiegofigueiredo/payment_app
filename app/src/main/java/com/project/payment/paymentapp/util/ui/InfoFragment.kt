package com.project.ifishing.util.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.project.payment.paymentapp.R
import kotlinx.android.synthetic.main.fragment_info.*
import kotlin.reflect.KFunction0

@SuppressLint("ValidFragment")
class InfoFragment(private val message: String,
                   private val textButton: String,
                   private val onButtonClicked: KFunction0<Unit>)
    : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tvMessage = tv_message
        tvMessage.text = message

        val tvTextButton = tv_text_button
        if (textButton.isNotEmpty()) {
            tvTextButton.text = textButton
            tvTextButton.setOnClickListener {
                onButtonClicked()
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commitNow()
            }
        } else {
            tvTextButton.visibility = View.GONE
        }
    }
}