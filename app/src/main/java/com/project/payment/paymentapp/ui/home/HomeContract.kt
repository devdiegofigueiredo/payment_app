package com.project.payment.paymentapp.ui.home

interface HomeContract {

    interface View {
        fun onInvalidValueInputed()
        fun onValidValueInputed()
    }

    interface Presenter {
        fun onValueInputed(value: String)
    }
}