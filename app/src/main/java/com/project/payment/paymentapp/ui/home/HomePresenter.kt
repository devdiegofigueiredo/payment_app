package com.project.payment.paymentapp.ui.home

class HomePresenter(val view: HomeContract.View) : HomeContract.Presenter {

    override fun onValueInputed(value: String) {
        if (value.isNotEmpty() && clearMoneyFormat(value).toInt() > 0) {
            view.onValidValueInputed()
        } else {
            view.onInvalidValueInputed()
        }
    }

    private fun clearMoneyFormat(value: String): String {
        return value.replace("[R$,.]".toRegex(), "")
    }
}