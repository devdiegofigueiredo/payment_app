package com.project.payment.paymentapp.ui.installments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.installments.domain.model.Installment
import kotlin.reflect.KFunction0

class InstallmentsAdapter(private val installments: List<Installment.Installments>,
                          private val onInstallmentSelected: KFunction0<Unit>,
                          private val context: Context) :
        RecyclerView.Adapter<InstallmentsAdapter.ViewHolder>() {

    var installmentSelected: Installment.Installments? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.installments_item, parent, false))

    override fun getItemCount(): Int = installments.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.radioButton.setOnClickListener {
            onInstallmentSelected()
            installmentSelected = installments[position]
            notifyDataSetChanged()
        }
        holder.radioButton.isChecked = installmentSelected?.installments == installments[position].installments
        holder.message.text = installments[position].message
        holder.installmentNumber.text = context.getString(R.string.installment_numbers).plus(installments[position].installments)
        holder.installmentValue.text = context.getString(R.string.installment)
                .plus(String.format("%.2f", installments[position].value.toDouble()))
        holder.totalValue.text = context.getString(R.string.total_value)
                .plus(String.format("%.2f", installments[position].total.toDouble()))
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioButton: RadioButton = itemView.findViewById(R.id.radio_button)
        val message: TextView = itemView.findViewById(R.id.recommended_message)
        val installmentNumber: TextView = itemView.findViewById(R.id.installment_number)
        val installmentValue: TextView = itemView.findViewById(R.id.parcel_value)
        val totalValue: TextView = itemView.findViewById(R.id.total_value)
    }
}