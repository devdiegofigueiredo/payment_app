package com.project.payment.paymentapp.ui.paymentmethod

import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod

interface PaymentMethodContract {

    interface View {
        fun setupPaymentMethods(paymentMethods: List<PaymentMethod>)
        fun showErrorView(message: String, buttonTitle: String)
        fun showProgress()
        fun hideProgress()
        fun startSelectBankScreen(paymentMethodId: String)
        fun showEmptySelectedPaymentMethodMessage()
    }

    interface Presenter {
        interface PaymentMethodCallback {
            fun onPaymentMethodSuccess(paymentMethods: List<PaymentMethod>)
            fun onPaymentMethodError(message: String, buttonTitle: String)
            fun onPaymentConfirmed(paymentMethodSelected: String)
        }

        fun paymentMethods()
    }

    interface Interactor {
        fun paymentMethod(callback: Presenter.PaymentMethodCallback)
    }
}