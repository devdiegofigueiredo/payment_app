package com.project.payment.paymentapp.ui.installments

import com.project.payment.paymentapp.ui.installments.domain.model.Installment

interface InstallmentsContract {

    interface View {
        fun showErrorView(message: String, buttonTitle: String)
        fun showProgress()
        fun hideProgress()
        fun setupInstallments(installmentList: List<Installment>)
        fun showPaymentInformationDialog(installmentSelected: Installment.Installments)
        fun reestartPaymentFlow()
    }

    interface Presenter {
        interface InstallmentsCallback {
            fun onInstallmentsSuccess(installments: List<Installment>)
            fun onInstallmentsError(message: String, buttonTitle: String)
        }

        fun installments(paymentMethod: String, bankId: String, amount: String)
        fun onInstallmentsConfirmed(installmentSelected: Installment.Installments)
        fun onPaymentInformationDialogConfirmed()
    }

    interface Interactor {
        fun installments(callback: Presenter.InstallmentsCallback,
                         paymentMethod: String,
                         bankId: String,
                         amount: String)
    }
}