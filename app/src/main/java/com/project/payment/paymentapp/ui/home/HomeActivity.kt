package com.project.payment.paymentapp.ui.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.paymentmethod.PaymentMethodActivity
import com.project.payment.paymentapp.util.PhoneMask
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), HomeContract.View {

    private lateinit var presenter: HomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        value.addTextChangedListener(PhoneMask.moneyMask(value))
        presenter = HomePresenter(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_done, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                presenter.onValueInputed(value.text.toString())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onInvalidValueInputed() {
        value.requestFocus()
        value.error = getString(R.string.input_valid_value)
    }

    override fun onValidValueInputed() {
        val intent = Intent(this, PaymentMethodActivity::class.java)
        intent.putExtra(PaymentMethodActivity.EXTRA_AMOUNT, value.text.toString())
        startActivity(intent)
    }
}
