package com.project.payment.paymentapp.ui.banks

import android.content.Context
import com.project.payment.paymentapp.BuildConfig
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.data.remote.BaseService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class BanksInteractor(val context: Context) : BanksContract.Interactor {

    override fun banks(callback: BanksContract.Presenter.BanksCallback, paymentMethod: String) {
        val banks = BaseService.banks(BuildConfig.public_key, paymentMethod)
        banks.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.isNotEmpty()) {
                            callback.onBanksSuccess(this)
                        } else {
                            callback.onBanksEmpty()
                        }
                    }
                }, {
                    callback.onBanksError(context.getString(R.string.error_retrieve_banks),
                            context.getString(R.string.try_again))
                })
    }
}