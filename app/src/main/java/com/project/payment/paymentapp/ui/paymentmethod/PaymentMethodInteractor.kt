package com.project.payment.paymentapp.ui.paymentmethod

import android.content.Context
import com.project.payment.paymentapp.BuildConfig
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.data.remote.BaseService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PaymentMethodInteractor(val context: Context) : PaymentMethodContract.Interactor {

    override fun paymentMethod(callback: PaymentMethodContract.Presenter.PaymentMethodCallback) {
        val callCreateUser = BaseService.paymentMethods(BuildConfig.public_key)
        callCreateUser.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.isNotEmpty()) {
                            callback.onPaymentMethodSuccess(this)
                        }
                    }
                }, {
                    callback.onPaymentMethodError(context.getString(R.string.error_retrieve_payment_methods),
                            context.getString(R.string.try_again))
                })
    }
}