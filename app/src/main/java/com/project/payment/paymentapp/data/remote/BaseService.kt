package com.project.payment.paymentapp.data.remote

import com.project.payment.paymentapp.BuildConfig
import com.project.payment.paymentapp.ui.banks.domain.model.Bank
import com.project.payment.paymentapp.ui.installments.domain.model.Installment
import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BaseService {

    companion object {

        private fun getRetrofitInstance(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }

        private fun instance(baseUrl: String): Retrofit {
            val instance: Retrofit by lazy { getRetrofitInstance(baseUrl) }
            return instance
        }

        fun paymentMethods(publicKey: String): Observable<List<PaymentMethod>> {
            return instance(BuildConfig.base_url).create(PaymentMethodService::class.java).paymentMethods(publicKey).flatMap { paymentMethods ->
                Observable.fromCallable { paymentMethods }
            }.map { response -> response }
        }

        fun banks(publicKey: String, paymentMethod: String): Observable<List<Bank>> {
            return instance(BuildConfig.base_url).create(PaymentMethodService::class.java).banks(publicKey, paymentMethod).flatMap { paymentMethods ->
                Observable.fromCallable { paymentMethods }
            }.map { response -> response }
        }

        fun installments(publicKey: String, paymentMethod: String, bankId: String, amount: String): Observable<List<Installment>> {
            return instance(BuildConfig.base_url).create(PaymentMethodService::class.java).installments(publicKey, paymentMethod, bankId, amount).flatMap { paymentMethods ->
                Observable.fromCallable { paymentMethods }
            }.map { response -> response }
        }
    }
}