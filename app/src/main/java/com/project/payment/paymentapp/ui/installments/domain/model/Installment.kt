package com.project.payment.paymentapp.ui.installments.domain.model

import com.google.gson.annotations.SerializedName

data class Installment(@SerializedName("payer_costs") val installments: List<Installments>) {

    data class Installments(val installments: String,
                            @SerializedName("installment_amount") val value: String,
                            @SerializedName("recommended_message") val message: String,
                            @SerializedName("total_amount") val total: String)
}