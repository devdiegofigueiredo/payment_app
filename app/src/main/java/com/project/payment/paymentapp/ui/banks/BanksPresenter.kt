package com.project.payment.paymentapp.ui.banks

import com.project.payment.paymentapp.ui.banks.domain.model.Bank

class BanksPresenter(val view: BanksContract.View,
                     val interactor: BanksContract.Interactor) :
        BanksContract.Presenter,
        BanksContract.Presenter.BanksCallback {

    override fun banks(paymentMethod: String) {
        view.showProgress()
        interactor.banks(this, paymentMethod)
    }

    override fun onBankConfirmed(bank: String) {
        if (bank.isNotEmpty()) {
            view.startSelectParcelsScreen(bank)
        } else {
            view.showEmptySelectedPaymentMethodMessage()
        }
    }

    override fun onBanksSuccess(banks: List<Bank>) {
        view.setupBanks(banks)
        view.hideProgress()
    }

    override fun onBanksError(message: String, buttonTitle: String) {
        view.showErrorView(message, buttonTitle)
        view.hideProgress()
    }

    override fun onBanksEmpty() {
        view.showEmptyBanksMessage()
    }
}