package com.project.payment.paymentapp.ui.banks

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.project.ifishing.util.ui.InfoFragment
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.banks.domain.model.Bank
import com.project.payment.paymentapp.ui.installments.InstallmentsActivity
import com.project.payment.paymentapp.util.ProgressDialogUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_banks.*

class BanksActivity : AppCompatActivity(), BanksContract.View {

    private lateinit var progressDialog: AlertDialog
    private lateinit var presenter: BanksPresenter
    private lateinit var adapter: BanksAdapter
    private lateinit var paymentMethod: String
    private lateinit var amount: String

    private var isBankdSelected = false

    companion object {
        val EXTRA_PAYMENT_METHOD = "extra_payment_method"
        val EXTRA_AMOUNT = "extra_amount"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_banks)

        setupToolbar()

        progressDialog = ProgressDialogUtil.getDialog(this.getString(R.string.looking_banks), this)

        paymentMethod = intent.extras.getString(EXTRA_PAYMENT_METHOD)
        amount = intent.extras.getString(EXTRA_AMOUNT)

        val interactor = BanksInteractor(this)
        presenter = BanksPresenter(this, interactor)
        presenter.banks(paymentMethod)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (isBankdSelected) {
            menuInflater.inflate(R.menu.menu_done, menu)
        } else {
            menuInflater.inflate(R.menu.menu_done_disabled, menu)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                presenter.onBankConfirmed(adapter.bankSelected)
            }
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun startSelectParcelsScreen(bank: String) {
        val intent = Intent(this, InstallmentsActivity::class.java)
        intent.putExtra(InstallmentsActivity.EXTRA_PAYMENT_METHOD, paymentMethod)
        intent.putExtra(InstallmentsActivity.EXTRA_BANK, adapter.bankSelected)
        intent.putExtra(InstallmentsActivity.EXTRA_AMOUNT, amount)
        startActivity(intent)
    }

    override fun showProgress() {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    override fun hideProgress() {
        if (progressDialog.isShowing) {
            progressDialog.hide()
        }
    }

    override fun showErrorView(message: String, buttonTitle: String) {
        val errorFragment = InfoFragment(message, buttonTitle, this::onTryAgainClicked)
        supportFragmentManager?.beginTransaction()?.replace(R.id.rl_error_container, errorFragment)?.commitAllowingStateLoss()
    }

    override fun setupBanks(bankList: List<Bank>) {
        adapter = BanksAdapter(bankList, Picasso.get(), this::onBankSelected)
        banks.layoutManager = LinearLayoutManager(this)
        banks.adapter = adapter
    }

    override fun showEmptySelectedPaymentMethodMessage() {
        Toast.makeText(this, getString(R.string.select_bank), Toast.LENGTH_LONG).show()
    }

    override fun showEmptyBanksMessage() {
        Toast.makeText(this, getString(R.string.banks_not_found), Toast.LENGTH_LONG).show()
        finish()
    }

    private fun onTryAgainClicked() {
        presenter.banks(paymentMethod)
    }

    private fun onBankSelected() {
        isBankdSelected = true
        invalidateOptionsMenu()
    }

    private fun setupToolbar() {
        val toolbar = supportActionBar
        toolbar?.title = getString(R.string.banks)
        toolbar?.setDisplayHomeAsUpEnabled(true)
    }
}