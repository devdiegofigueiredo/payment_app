package com.project.payment.paymentapp.ui.paymentmethod.domain.model

data class PaymentMethod(val id: String,
                         val name: String,
                         val payment_type_id: String,
                         val thumbnail: String)