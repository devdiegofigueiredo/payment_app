package com.project.payment.paymentapp.ui.paymentmethod

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod
import com.squareup.picasso.Picasso
import kotlin.reflect.KFunction0

class PaymentMethodAdapter(private val paymentMethods: List<PaymentMethod>,
                           private val picasso: Picasso,
                           private val onPaymentMethodSelected: KFunction0<Unit>) :
        RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>() {

    var paymentMethodSelected = ""

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.payment_methods_item, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = paymentMethods[position].name
        holder.radioButton.isChecked = paymentMethodSelected == paymentMethods[position].id
        holder.radioButton.setOnClickListener {
            onPaymentMethodSelected()
            paymentMethodSelected = paymentMethods[position].id
            notifyDataSetChanged()
        }
        picasso.load(paymentMethods[position].thumbnail).into(holder.image)
    }

    override fun getItemCount(): Int = paymentMethods.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioButton: RadioButton = itemView.findViewById(R.id.radio_button)
        val name: TextView = itemView.findViewById(R.id.name)
        val image: ImageView = itemView.findViewById(R.id.image)
    }
}