package com.project.payment.paymentapp.ui.banks

import com.project.payment.paymentapp.ui.banks.domain.model.Bank

interface BanksContract {

    interface View {
        fun showProgress()
        fun hideProgress()
        fun startSelectParcelsScreen(bank: String)
        fun showErrorView(message: String, buttonTitle: String)
        fun setupBanks(bankList: List<Bank>)
        fun showEmptySelectedPaymentMethodMessage()
        fun showEmptyBanksMessage()
    }

    interface Presenter {
        interface BanksCallback {
            fun onBanksSuccess(banks: List<Bank>)
            fun onBanksError(message: String, buttonTitle: String)
            fun onBanksEmpty()
        }

        fun banks(paymentMethod: String)
        fun onBankConfirmed(bank: String)
    }

    interface Interactor {
        fun banks(callback: Presenter.BanksCallback, paymentMethod: String)
    }
}