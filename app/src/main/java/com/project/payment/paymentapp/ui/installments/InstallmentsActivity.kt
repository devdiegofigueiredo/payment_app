package com.project.payment.paymentapp.ui.installments

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.project.ifishing.util.ui.InfoFragment
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.home.HomeActivity
import com.project.payment.paymentapp.ui.installments.domain.model.Installment
import com.project.payment.paymentapp.util.ProgressDialogUtil
import kotlinx.android.synthetic.main.activity_installments.*

class InstallmentsActivity : AppCompatActivity(), InstallmentsContract.View {

    private lateinit var progressDialog: AlertDialog
    private lateinit var presenter: InstallmentsPresenter
    private lateinit var adapter: InstallmentsAdapter
    private lateinit var paymentMethod: String
    private lateinit var bankId: String
    private lateinit var amount: String
    private lateinit var dialogBuilder: AlertDialog
    private var isInstallmentsSelected = false

    companion object {
        val EXTRA_PAYMENT_METHOD = "extra_payment_method"
        val EXTRA_BANK = "extra_bank"
        val EXTRA_AMOUNT = "extra_amount"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installments)

        setupToolbar()

        paymentMethod = intent.extras.getString(EXTRA_PAYMENT_METHOD)
        bankId = intent.extras.getString(EXTRA_BANK)
        amount = intent.extras.getString(EXTRA_AMOUNT)

        progressDialog = ProgressDialogUtil.getDialog(this.getString(R.string.looking_payment_methods), this)

        val interactor = InstallmentsInteractor(this)
        presenter = InstallmentsPresenter(this, interactor)
        presenter.installments(paymentMethod, bankId, amount)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (isInstallmentsSelected) {
            menuInflater.inflate(R.menu.menu_done, menu)
        } else {
            menuInflater.inflate(R.menu.menu_done_disabled, menu)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                adapter.installmentSelected?.apply {
                    presenter.onInstallmentsConfirmed(this)
                }
            }
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showErrorView(message: String, buttonTitle: String) {
        val errorFragment = InfoFragment(message, buttonTitle, this::onTryAgainClicked)
        supportFragmentManager?.beginTransaction()?.replace(R.id.rl_error_container, errorFragment)?.commitAllowingStateLoss()
    }

    override fun showProgress() {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    override fun hideProgress() {
        if (progressDialog.isShowing) {
            progressDialog.hide()
        }
    }

    override fun setupInstallments(installmentList: List<Installment>) {
        adapter = InstallmentsAdapter(installmentList[0].installments, this::onInstallmentSelected, this)
        installments.layoutManager = LinearLayoutManager(this)
        installments.adapter = adapter
    }

    override fun showPaymentInformationDialog(installment: Installment.Installments) {
        val dialog = AlertDialog.Builder(this)

        val message = getString(R.string.installment_numbers).plus(installment.installments) + "\n" +
                getString(R.string.installment).plus(String.format("%.2f", installment.value.toDouble())) + "\n" +
                getString(R.string.total_value).plus(String.format("%.2f", installment.total.toDouble()))

        dialog.setMessage(message)
        dialog.setPositiveButton(getString(R.string.ok), { _, _ ->
            presenter.onPaymentInformationDialogConfirmed()
        })
        dialog.setNegativeButton(getString(R.string.cancel), { _, _ ->
            dialogBuilder.dismiss()
        })
        dialogBuilder = dialog.create()
        dialogBuilder.setCancelable(true)
        dialogBuilder.show()
    }

    override fun reestartPaymentFlow() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    private fun onTryAgainClicked() {
        presenter.installments(paymentMethod, bankId, amount)
    }

    private fun setupToolbar() {
        val toolbar = supportActionBar
        toolbar?.title = getString(R.string.installments)
        toolbar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun onInstallmentSelected() {
        isInstallmentsSelected = true
        invalidateOptionsMenu()
    }
}