package com.project.payment.paymentapp.ui.paymentmethod

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.project.ifishing.util.ui.InfoFragment
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.banks.BanksActivity
import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod
import com.project.payment.paymentapp.util.ProgressDialogUtil
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_payment_method.*

class PaymentMethodActivity : AppCompatActivity(), PaymentMethodContract.View {

    private lateinit var presenter: PaymentMethodPresenter
    private lateinit var progressDialog: AlertDialog
    private lateinit var adapter: PaymentMethodAdapter
    private lateinit var amount: String
    private var isPaymentMethodSelected = false

    companion object {
        val EXTRA_AMOUNT = "extra_amount"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)

        progressDialog = ProgressDialogUtil.getDialog(this.getString(R.string.looking_payment_methods), this)
        setupToolbar()

        amount = intent.extras.getString(EXTRA_AMOUNT)

        val interactor = PaymentMethodInteractor(this)
        presenter = PaymentMethodPresenter(this, interactor)
        presenter.paymentMethods()
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (isPaymentMethodSelected) {
            menuInflater.inflate(R.menu.menu_done, menu)
        } else {
            menuInflater.inflate(R.menu.menu_done_disabled, menu)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                presenter.onPaymentConfirmed(adapter.paymentMethodSelected)
            }
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setupPaymentMethods(paymentMethods: List<PaymentMethod>) {
        adapter = PaymentMethodAdapter(paymentMethods, Picasso.get(), this::onPaymentMethodSelected)
        val paymentMethodsList = payment_methods
        paymentMethodsList.layoutManager = LinearLayoutManager(this)
        paymentMethodsList.adapter = adapter
    }

    override fun showErrorView(message: String, buttonTitle: String) {
        val errorFragment = InfoFragment(message, buttonTitle, this::onTryAgainClicked)
        supportFragmentManager?.beginTransaction()?.replace(R.id.rl_error_container, errorFragment)?.commitAllowingStateLoss()
    }

    override fun showProgress() {
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    override fun hideProgress() {
        if (progressDialog.isShowing) {
            progressDialog.hide()
        }
    }

    override fun startSelectBankScreen(paymentMethodId: String) {
        val intent = Intent(this, BanksActivity::class.java)
        intent.putExtra(BanksActivity.EXTRA_PAYMENT_METHOD, paymentMethodId)
        intent.putExtra(BanksActivity.EXTRA_AMOUNT, amount)
        startActivity(intent)
    }

    override fun showEmptySelectedPaymentMethodMessage() {
        Toast.makeText(this, getString(R.string.select_payment_method), Toast.LENGTH_LONG).show()
    }

    private fun onTryAgainClicked() {
        presenter.paymentMethods()
    }

    private fun setupToolbar() {
        val toolbar = supportActionBar
        toolbar?.title = getString(R.string.payment_method)
        toolbar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun onPaymentMethodSelected() {
        isPaymentMethodSelected = true
        invalidateOptionsMenu()
    }
}