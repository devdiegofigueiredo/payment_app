package com.project.payment.paymentapp.ui.installments

import android.content.Context
import com.project.payment.paymentapp.BuildConfig
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.data.remote.BaseService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InstallmentsInteractor(val context: Context) : InstallmentsContract.Interactor {

    override fun installments(callback: InstallmentsContract.Presenter.InstallmentsCallback,
                              paymentMethod: String,
                              bankId: String,
                              amount: String) {
        val installments = BaseService.installments(BuildConfig.public_key, paymentMethod, bankId, amount)
        installments.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.apply {
                        if (this.isNotEmpty()) {
                            callback.onInstallmentsSuccess(this)
                        }
                    }
                }, {
                    callback.onInstallmentsError(context.getString(R.string.error_retrieve_installments),
                            context.getString(R.string.try_again))
                })
    }
}