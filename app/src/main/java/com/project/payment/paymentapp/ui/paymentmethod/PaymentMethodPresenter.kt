package com.project.payment.paymentapp.ui.paymentmethod

import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod

class PaymentMethodPresenter(val view: PaymentMethodContract.View,
                             val interactor: PaymentMethodContract.Interactor) :
        PaymentMethodContract.Presenter,
        PaymentMethodContract.Presenter.PaymentMethodCallback {

    override fun paymentMethods() {
        view.showProgress()
        interactor.paymentMethod(this)
    }

    override fun onPaymentMethodSuccess(paymentMethods: List<PaymentMethod>) {
        view.setupPaymentMethods(paymentMethods)
        view.hideProgress()
    }

    override fun onPaymentMethodError(message: String, buttonTitle: String) {
        view.showErrorView(message, buttonTitle)
        view.hideProgress()
    }

    override fun onPaymentConfirmed(paymentMethodSelected: String) {
        if (paymentMethodSelected.isNotEmpty()) {
            view.startSelectBankScreen(paymentMethodSelected)
        } else {
            view.showEmptySelectedPaymentMethodMessage()
        }
    }
}