package com.project.payment.paymentapp.ui.banks

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.project.payment.paymentapp.R
import com.project.payment.paymentapp.ui.banks.domain.model.Bank
import com.squareup.picasso.Picasso
import kotlin.reflect.KFunction0

class BanksAdapter(private val banks: List<Bank>,
                   private val picasso: Picasso,
                   private val onPaymentMethodSelected: KFunction0<Unit>) :
        RecyclerView.Adapter<BanksAdapter.ViewHolder>() {

    var bankSelected = ""

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.payment_methods_item, parent, false))

    override fun getItemCount(): Int = banks.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = banks[position].name
        holder.radioButton.isChecked = bankSelected == banks[position].id
        holder.radioButton.setOnClickListener {
            onPaymentMethodSelected()
            bankSelected = banks[position].id
            notifyDataSetChanged()
        }
        picasso.load(banks[position].thumbnail).into(holder.image)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioButton: RadioButton = itemView.findViewById(R.id.radio_button)
        val name: TextView = itemView.findViewById(R.id.name)
        val image: ImageView = itemView.findViewById(R.id.image)
    }
}