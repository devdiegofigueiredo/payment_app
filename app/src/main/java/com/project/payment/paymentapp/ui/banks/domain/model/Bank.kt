package com.project.payment.paymentapp.ui.banks.domain.model

data class Bank(val id: String, val name: String, val thumbnail: String)