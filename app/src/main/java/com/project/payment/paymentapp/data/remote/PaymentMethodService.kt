package com.project.payment.paymentapp.data.remote

import com.project.payment.paymentapp.BuildConfig
import com.project.payment.paymentapp.ui.banks.domain.model.Bank
import com.project.payment.paymentapp.ui.installments.domain.model.Installment
import com.project.payment.paymentapp.ui.paymentmethod.domain.model.PaymentMethod
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PaymentMethodService {

    @GET(BuildConfig.payment_method_api)
    fun paymentMethods(@Query(value = "public_key") publicKey: String): Observable<List<PaymentMethod>>

    @GET(BuildConfig.banks_api)
    fun banks(@Query(value = "public_key") publicKey: String,
              @Query(value = "payment_method_id") paymentMethod: String):
            Observable<List<Bank>>

    @GET(BuildConfig.installments_api)
    fun installments(@Query(value = "public_key") publicKey: String,
                     @Query(value = "payment_method_id") paymentMethod: String,
                     @Query(value = "issuer.id") issuerId: String,
                     @Query(value = "amount") amount: String):
            Observable<List<Installment>>

}