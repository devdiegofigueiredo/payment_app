package com.project.payment.paymentapp.util

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.ProgressBar
import android.widget.TextView
import com.project.payment.paymentapp.R

class ProgressDialogUtil {

    companion object {

        fun getDialog(message: String, context: Context): AlertDialog {
            return createDialog(message, context)
        }

        private fun createDialog(message: String?, context: Context): AlertDialog {
            val dialog = AlertDialog.Builder(context).create()
            val inflater = LayoutInflater.from(context)
            val view = inflater.inflate(R.layout.progress_dialog, null, false)

            message?.apply {
                val textMessage = view.findViewById<TextView>(R.id.message)
                textMessage.text = message
            }

            val progressBar = view.findViewById<ProgressBar>(R.id.pb_progress)
            progressBar.indeterminateDrawable.setColorFilter(ContextCompat.getColor(context, android.R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY)

            dialog.setView(view)
            dialog.setCancelable(true)
            return dialog
        }
    }
}